# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Carousel, ImageButton, VisitorDetail

admin.site.register(Carousel)
admin.site.register(ImageButton)
admin.site.register(VisitorDetail)
