# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from validators import validate_image

class Carousel(models.Model):
	"""
	A model for storing the carousel related data
	"""
	title = models.CharField(max_length=30, null=False, blank=False)
	image = models.ImageField(validators=[validate_image])
	description = models.CharField(max_length=60, null=True, blank=True)

	def __str__(self):
		return "CAROUSEL ITEM %d - %s"%(self.id, self.title)

class ImageButton(Carousel):
	"""
	A model for storing the image button related data
	"""
	def __str__(self):
		return "IMAGE BUTTON %d - %s"%(self.id, self.title)


class VisitorDetail(models.Model):
	ip_address = models.CharField(max_length=50, unique=True)
	details = models.CharField(max_length=1000, default="")
	visit_count = models.BigIntegerField(default=1)
	visited_at = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __str__(self):
		return "IP " + self.ip_address + ", visited HOME page at " + str(self.visited_at)





