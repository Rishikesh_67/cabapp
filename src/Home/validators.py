from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions
        
def validate_image(image):
	print "Got image as ", image
	w, h = get_image_dimensions(image)
	print "Uploading an image with width: ", w, " and height: ",h

	if w < 400:
		raise ValidationError("Image width should be more than 700px, your is "+str(w)+"px")
	if h < 250:
		raise ValidationError("Image height should be more than 500px, your is "+str(h)+"px")
	
	print "Uploaded image's Width: ", w, " and Height: ", h

