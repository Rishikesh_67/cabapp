# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from Register.models import User
import validators 
from Login.validators import validate_contact

class Faq(models.Model):
	questioned_by = models.CharField(max_length=100, null=False, blank=False)
	contact = models.PositiveIntegerField(validators= [validate_contact] ,blank=False, null=False, unique=True)	#Required 
	email = models.EmailField(max_length=50, default="", blank=True) #Optional
	question = models.TextField(null=False, blank=False, validators=[validators.validate_question])
	questioned_at = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __str__(self):
		return "Question %d by %s"%(self.id, self.questioned_by)

	class Meta:
		verbose_name = "Old FAQ"


class NewFaq(models.Model):
	''' FAQs '''
	question = models.CharField(max_length=300)
	answer = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __str__(self):
		return "%s"%(self.question)

	class Meta:
		verbose_name = "New original frequently asked question"