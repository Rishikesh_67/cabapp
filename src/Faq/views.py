# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from models import NewFaq
from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.decorators import api_view
import json
# from Login.models import User

@csrf_exempt
def get_faqs(request):
	questions = NewFaq.objects.all().order_by("created_at")
	print questions
	return render(request, "faq.html", {"questions": questions})


# @api_view(["POST"])
# def store_faqs(request, format=None):
# 	try:
# 		print request.data, type(request.data)
# 		data_str = ""

# 		for k,v in request.data.iteritems():
# 			data_str = k

# 		if data_str == "":
# 			print "You forgot to provide informations(In Web View)"
# 			return Response({"message": "You forgot to provide informations", "status": 400}, status=400)

# 		print k, type(k)
# 		data = json.loads(k)
# 		keys = ["fname", "email", "contact", "question"]

# 		for key in keys:
# 			if not key in data:
# 				print "All information are required(fname, contact, email, question)"
# 				return Response({"message": "All information are required(fname, contact, email, question)", "status": 400}, status=400)

		
# 		print "Converting string data to an object"
# 		print "DATA => ", data

# 		fname = data["fname"]
# 		contact = data["contact"]
# 		email = data["email"]
# 		question = data["question"]

# 		faq = Faq(question=question, contact=contact, email=email, questioned_by=fname)
# 		faq.save()

# 		print "The posted question successfully stored"
# 		return Response({"message": "Your question has been successfully posted", "status": 200}, status=200)
# 	except Exception as e:
# 		print "Something unexpected occured on server side, ", e
# 		return Response({"status":400 ,	 "message": "Something unexpected occured on server side"}, status=400)


