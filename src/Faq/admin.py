# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from models import Faq, NewFaq

admin.site.register(Faq)
admin.site.register(NewFaq)
