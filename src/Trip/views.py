# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def trips(request, slug):
	if slug == "weekends":
		print ""
		return render(request, "weekends.html", {})
	elif slug == "popular-places":
		return render(request, "popular_places.html", {})
	elif slug == "pilgrimages":
		return render(request, "pilgrimages.html", {})
	else:
		return render(request, "error.html", {})


