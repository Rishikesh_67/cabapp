# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.core.validators import RegexValidator
import validators
from django.utils import timezone
import datetime

class User(models.Model):
	""" A class that defines the structure of User object """
	fname = models.CharField(max_length=50, default="", blank=True)	#Required
	lname = models.CharField(max_length=50, default="", blank=True)		#Optional
	contact = models.PositiveIntegerField(validators= [validators.validate_contact] ,blank=False, null=False, unique=True)	#Required 
	email = models.EmailField(max_length=50, default="", blank=True) #Optional
	address = models.TextField(default="India")
	is_active = models.BooleanField(default=True)
	is_admin = models.BooleanField(default=False)
	profile_pic = models.ImageField(default="user.png", max_length=200)
	app_key = models.TextField(default="", blank=True)
	otp = models.IntegerField(default=0, blank=True)
	password = models.TextField(max_length=20, default="", blank=True, null=True)
	otp_created_at = models.DateTimeField(default=None, blank=True, null=True)	#default=timezone.now
	

	def __unicode__(self):
		# +91 73537 87704
		# print self.contact, type(self.contact), str(self.contact), type(str(self.contact))
		if self.fname != "" or self.lname != "":
			full_name = self.fname + " " + self.lname
			# return "%100s %s %12d"%(full_name, " "*3, self.contact)
			return str(self.id)+ " - " +full_name + ",   " + "+91 " + str(self.contact)[:5] + " " + str(self.contact)[5:]
		# return "%100s %s %12d"%("---", " "*3, self.contact)
		return str(self.id)+ " - " + "+91 " + str(self.contact)[:5] + " " + str(self.contact)[5:]
 


# class EngineeringUni(models.Model):
#     field2 = models.CharField(max_length=200)
#     des_eng = models.CharField(max_length=1000, default='Add description')

#     def __str__(self):
#         return self.field2

#     def description_eng_universities(self):
#         return self.des_eng


# class EngineeringCourse(models.Model):
#     course_name = models.CharField(max_length=400)

#     course_description = models.CharField(max_length=1000, default='This is a description')

#     course_offered_by = models.ManyToManyField(EngineeringUni, related_name='course_offered_by')

#     course_duration = models.IntegerField(blank=False, default='2')

#     def __str__(self):
#         return self.course_name

#     def description_course(self):
#         return self.course_description

#     def offered_by_courses(self):
#         return self.course_offered_by

#     def duration_courses(self):
#         return str(self.course_duration)
