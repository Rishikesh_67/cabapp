# -*- coding: utf-8 -*-
from __future__ import unicode_literals 
from django.shortcuts import render
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
# from rest_framework.settings import api_settings
import hashlib
import re
import json
import services
from models import User 
import jwt
from django.utils import timezone
from rest_framework import viewsets
from .models import User
from .serializers import UserSerializer
from rest_framework.views import APIView
from Home.models import VisitorDetail
# import ..conf

@api_view(['POST'])
# @renderer_classes((JSONRenderer,))
def send_otp(request, format=None):
    """
        A view that returns an object otp, and status (if data is valid) in JSON
    """
    try:
        data = request.data
        mobile_number = ""
        app_key = ""

        print "Request Data: ", data
        print json.dumps(data, indent=4)
        
        # For browser request
        if not "mob" in data and not "app_key" in data:
            for k, v in data.iteritems():
                browser_data_str = k
                break
            print "Browser request, got ", browser_data_str, type(browser_data_str)

            browser_data = json.loads(browser_data_str)

            if not "mob" in browser_data or not "app_key" in browser_data:
                return Response(
                    {"status": 400, "message": "App key & mobile number both are required"})
 
            print browser_data["mob"]
            mobile_number = browser_data["mob"].strip()
            app_key = browser_data["app_key"].strip()
            print "Got"
        else:
            # if not "mob" in data or not "app_key" in data:
            if not "mob" in data or not "app_key" in data:
                return Response(
                    {"status": 400, "message": "App key & mobile number both are required"})

            mobile_number = data["mob"].strip()
            app_key = data["app_key"].strip()

        print "Got Mobile number.   : ", mobile_number
        print "Got app key(frontend): ", app_key

        if not re.match("^([789])([0-9]{9})$", mobile_number):
            print "Mobile number should contain 10 digits & start with 7/8/9"
            return Response({
                   "status": 400, 
                    "message": "Mobile number should contain 10 digits & start with 7/8/9"
                },
                status=400
            )

        server_app_key = hashlib.md5(mobile_number + "offlinetrend").hexdigest()

        print "Server side's app key: ", server_app_key

        if app_key != server_app_key:
            print "Server side app key didn't match with Client side app key"
            return Response({
                    "status": 401, 
                    "message": "App key does not correspond to the mobile number"
                }, 
                status=401
            )

        print "Checking mobile number on database"
        data = {}
        is_new = False
        user = User()
        try:
            user = User.objects.get(contact=mobile_number)
            data.update({"is_new": is_new})
            print "User exists with this mobile number %s" % (mobile_number)
        except BaseException:
            print "Mobile number does not exist into the database"
            user = User(contact=mobile_number, app_key=app_key)
            user.save()
            is_new = True
            data.update({"is_new": is_new})
            print "Created new entry with mobile number ", mobile_number
            

        # If user is active then send the OTP
        if user.is_active:
            #For new users
            if is_new:
                 services.sms(user, mobile_number, data)
            else:
                # otp_created_at = user.otp_created_at
                # otp = user.otp

                # print "Last otp was: ", otp
                # "And that was created at ", otp_created_at

                # current_time = timezone.now()
                # diff = current_time - otp_created_at

                # print "Current time: ", current_time
                # print "Difference(", current_time,",",otp_created_at,"): ", diff

                # seconds = diff.total_seconds() 
                # print "Difference in seconds: ", seconds
                # minutes = seconds/60
                # print "Difference in minutes: ", minutes

                minutes = services.get_otp_creation_time(user)   # in minutes

                if minutes <= 2:
                    print "Sending the previous otp again"
                    services.sms(user, mobile_number, data, user.otp)
                else:
                    print "Sending new otp"
                    services.sms(user, mobile_number, data)
                print data
        else:
            data.update({
            	"status": 200, 
            	"is_active": False,
            	"message": "In active users can't receive otp"
            })
            print "User with mobile number %s is NOT ACTIVE" % (mobile_number)
            print data

        return Response(data)	

    except BaseException as e:  
        print "Error:", e
        return Response({ "status": 500, "message": "Something unexpected happended", "error": str(e)}, status=500)


# view to get token
@api_view(["POST"])
def get_token(request, format=None):
    """
        A view that returns jwt (once otp & app_key got validated) in JSON
    """
    # print "OLA", conf.
    try:
        data = request.data

        print "Request Data(Token request): ", data
        print json.dumps(data, indent=4) 

        # if not "otp" in data or not "app_key" in data:
        # if not "otp" in data or not "app_key" in data or not "user_id" in data:
        #     print "It seems client didn't send the required data. App key, User id & OTP all are required to get the token"
        #     return Response(
        #         {"status": 400, "message": "App key, User id & OTP all are required to get the token"})
        
        # otp = data["otp"].strip()
        # app_key = data["app_key"].strip()
        # user_id = data["user_id"].strip()
        otp = 0
        app_key = ""
        user_id = 0

        if not "otp" in data and not "app_key" in data and not "user_id" in data:
            print "Iterating over items"
            for k, v in data.iteritems():
                browser_data_str = k
                break
            print "Browser request, got ", browser_data_str, type(browser_data_str)

            browser_data = json.loads(browser_data_str)

            print "Successfully created Dictionary"

            if not "otp" in browser_data or not "app_key" in browser_data or not "user_id" in browser_data:
                return Response(
                    {"status": 400, "message": "App key & Otp and UserId, all are required"})
 
            print browser_data["otp"]
            otp = browser_data["otp"]
            app_key = browser_data["app_key"].strip()
            user_id = browser_data["user_id"]
            print "Fetched ", otp, app_key, user_id, " from Browser POST Data" 
        else:
            # if not "mob" in data or not "app_key" in data:
            if not "otp" in data or not "app_key" in data or not "user_id" in data:
                return Response(
                    {"status": 400, "message": "App key & Otp and UserId, all are required"})

            otp = data["otp"].strip()
            app_key = data["app_key"].strip()
            user_id = data["user_id"].strip()

        # It is required to first check the user_id before getting user data from db
        try:
            int(user_id)    #ValueError for user_id = "23.5", "fgh" etc
        except:
            print "User_id should be an integer"
            return Response(
                    {"status": 400, "message": "user_id should be an integer"}, status=400)

        if len(otp) != 6:
            print "OTP does not seem to contain 6 digits"
            return Response(
                {"status": 400, "message": "OTP does not seem to contain 6 digits"})

        user = User()

        # try:
        #     user = User.objects.get(app_key=app_key)
        # except User.DoesNotExist:
        #     return Response(
        #         {"status": 400, "message": "Couldn't found app key, you need to register"})
        

        try:
            user = User.objects.get(id=int(user_id))
            print "Got user with id ", user_id
        except User.DoesNotExist:
            print "Could not found the user with user id ", user_id
            return Response(
                {"status": 400, "message": "Couldn't found user with user id %s"%(user_id)}, status=400)
         
        # try:
        #     user = User.objects.get(app_key=app_key, otp=otp)
        # except User.DoesNotExist:
        #     return Response(
        #         {"status": 400, "message": "Couldn't found otp corresponding to app key, please check your otp"},status=400)

        server_app_key = hashlib.md5(str(user.contact) + "offlinetrend").hexdigest()

        print "Server generated app key(Token request): ", server_app_key
        print "Client's app key                       : ", app_key

        if server_app_key != app_key:
            return Response(
                {"status": 400, "message": "App key didn't match for user id %s, make sure user id should correspond to app key"%(user_id)}, status=400)

        if user.otp == 0:
            return Response({"status": 400, "message": "You have not requested for any OTP"}, status=400)

        if otp != str(user.otp):
            print "OTP didn't match"
            return Response({"status": 400, "message": "OTP didn't match"}, status=400)

        minutes = services.get_otp_creation_time(user)
        if minutes >= 2:
            print "Your OTP got expired"
            return Response({"status": 400, "message": "Your OTP got expired(2 minutes over), login with your mobile number"}, status=400)
        else:
            print "Your OTP has not been expired"
         
        return Response({ "token": services.create_jwt(user, app_key), "status": 200}, status=200)
    except BaseException as e:
        return Response({ "status": 500, "message": "Something unexpected happended", "error": str(e)}, status=500)

# user_count = User.objects.filter(active=True).count()
# content = {'user_count': user_count}

class UsersAPIView(APIView):
    """
        List all users (Used by Admin)
    """
    def get(self, request, format=None):
        try:
            d = request.META
            print d
            for k in d:
                print k
            user_id = request.META.get("HTTP_USERID")

            if not user_id: #jwt will be None(if it does not exist in the request header)
                return  Response({"status":400, "message": "USER_ID is required"}, status=400) 
            else:
                user_id = user_id.strip()

            uid, err_response = services.user_id_is_valid(user_id)

            if err_response:
                return err_response

            jwt_str, err_response = services.extract_jwt_from_header(request)

            if err_response:
                return err_response

            user = User.objects.get(id=uid)
            print "User with user id ",uid, " found"

            try:
                jwt.decode(jwt_str, user.app_key)    # Raises Error if JWT is expired
                print "JWT successfully got validated"
            except:
                print "JWT is not valid, request it again"
                return Response({"status": 401, "message": "JWT got expired, authentication failed. OTP request(Login) is required to get new JWT"}, status=401)

            # serializer = UserSerializer(user)
            users = User.objects.all()
            serializer = UserSerializer(users, many=True)
            print "Returning list of all users"
            return Response(serializer.data)
        except User.DoesNotExist:
            print "User with this user id does not exist"
            return Response({"status": 400, "message": "user with this user id does not exist"}, status=400)
        except:
            print "Unknown Server Error"
            return Response({"status": 500, "message": "Server Error"}, status=500)

class UserAPIView(APIView):
    """
        List specific user
    """
    def get(self, request, id, format=None):
        try:

            uid, err_response = services.user_id_is_valid(id)

            if err_response:
                return err_response

            jwt_str, err_response = services.extract_jwt_from_header(request)

            if err_response:
                return err_response

            user = User.objects.get(id=uid)
            print "User with user id ",uid, " found"

            try:
                jwt.decode(jwt_str, user.app_key)    # Raises Error if JWT is expired
                print "JWT successfully got validated"
            except:
                print "JWT is not valid, request it again"
                return Response({"status": 401, "message": "JWT got expired, authentication failed. OTP request(Login) is required to get new JWT"}, status=401)

            serializer = UserSerializer(user)
            return Response(serializer.data)
        except User.DoesNotExist:
            print "User with this user id does not exist"
            return Response({"status": 400, "message": "user with this user id does not exist"}, status=400)
        except:
            print "Unknown Server Error"
            return Response({"status": 500, "message": "Server Error"}, status=500)


@api_view(["POST"])
def update_profile(request, id,format=None):
    """ 
        A view that updates the user profile
    """
    uid, err_response = services.user_id_is_valid(id)

    if err_response:
        return err_response

    try:
        # print request.META
        # authorization = request.META.get("HTTP_AUTHORIZATION")

        # if not authorization: #jwt will be None(if it does not exist in the request header)
        #     return Response({"status":400, "message": "JWT is required"}, status=400)
        # else:
        #     authorization = authorization.strip()

        # jwt_str_arr = authorization.split()

        # if len(jwt_str_arr) != 2 or jwt_str_arr[0] != "Bearer":
        #     print "Authorization header should have exactly 2 parts like 'Bearer <jwt_token>'"
        #     return Response({"status": 400, "message": "Authorization header should have exactly 2 parts like 'Bearer <jwt_token>'"}, status=400)

        # jwt_str = jwt_str_arr[1]

        # print "Got JWT as ", jwt_str

        jwt_str, err_response = services.extract_jwt_from_header(request)

        if err_response:
            return err_response

        try:
            user = User.objects.get(id=uid)
            print "User with user id ",uid, " found" 
        except:
            print "User with this user id does not exist"
            return Response({"status": 400, "message": "user with this user id does not exist"}, status=400)

        app_key = user.app_key

        print "Successfully fetched the app_key from DB as ", app_key
        data = request.data
        print "Request Data: ", data

        # for k, v in request.META.iteritems():
        #     print k, "=", v

        # fname = ""
        # lname = ""
        # contact = ""
        # email = ""
        # address = ""
        # profile_pic = ""
        try:
            jwt.decode(jwt_str, app_key)    # Raises Error if JWT is expired
            print "JWT successfully got validated"
        except:
            print "JWT is not valid, request it again"
            return Response({"status": 401, "message": "JWT got expired, authentication failed. OTP request(Login) is required to get new JWT"}, status=401)
    

        data_dict = {}
            
        keys = ["fname", "lname", "email", "address", "mob", "profile_pic"]

        for k, v in data.iteritems():
            print k, v
            if k not in keys:
                print "Got inappropriate data"
                return Response({ "status": 400, "message": "Got inappropriate data"}, status=400)
            data_dict[k] = v

        print "Collected data: ", data_dict

        # user.update(**data_dict)
        User.objects.filter(id=uid).update(**data_dict)
        
        return Response({"status": 200, "message": "profile successfully updated"}, status=200)
    except Exception as e:
        print "Error occured"
        return Response({ "status": 500, "message": "Something unexpected happended", "error": str(e)}, status=500)


# A page that renders the standard home page
@csrf_exempt
def home(request):
    return render(request, "home.html", {})

# A page that renders the standard home page
@csrf_exempt
def home2(request):
    return render(request, "home/index.html", {})

# A page that renders the standard otp entering page
@csrf_exempt
def otp(request):
    return render(request, "otp.html", {}) 

@csrf_exempt
def book(request):
    return render(request, "book.html", {}) 

# UPDATED SCRATCH DEVELOPMENT

# @csrf_exempt
# def contact(request):
#     return render(request, "contact.html", {})

@csrf_exempt
def about(request):
    return render(request, "about.html", {})

@csrf_exempt
def book_new(request):
    return render(request, "book_new.html", {})


def update_visitor_details(request):
    """ Model definfition for visitor details """
    import platform

    visitors = "| ".join(platform.uname())

    import os
    # ip = os.environ["REMOTE_ADDR"]
    ip = ""
    try:
        ip = request.META.get("REMOTE_ADDR")
        print ">>>", request.META.get("HTTP_USER_AGENT")
        print ">>>", request.META.get("SERVER_NAME")
        # print ">>>", request.META.get("REMOTE_HOST")
    except:
        pass

    print visitors + " <> " + ip
    # if not ip == "127.0.0.1":
    # visitor = VisitorDetail.objects.get(ip_address=ip)
    try:
        print "Checking for IP availability"
        # visitor = VisitorDetail.objects.get(ip_address=ip)
        visitor = VisitorDetail.objects.get(ip_address=ip)
        print "Already there"
        visitor.visit_count = visitor.visit_count + 1
        visitor.save()
    except Exception as e:
        print e
        print "New Device visited, creating new ip entry"
        vistor = VisitorDetail.objects.create(ip_address=ip)
        vistor.save()

@csrf_exempt
def home_new(request):
    import threading

    threading.Thread(target=update_visitor_details, args=[request]).start()

    minutes = []
    for i in range(0, 60):
        if len(str(i)) == 1:
            minutes.append("0"+str(i))
        else:
            minutes.append(str(i))

    print minutes

    hours = []
    for i in range(1,13):
        if len(str(i)) == 1:
            hours.append("0"+str(i))
        else:
            hours.append(str(i))

    return render(request, "home_new.html", {"hours": hours, "minutes": minutes})

@csrf_exempt
def login_new(request):
    return render(request, "login.html", {})

@csrf_exempt
def trial(request):
    return render(request, "trial.html", {})

@csrf_exempt
def comments(request):
    return render(request, "comments.html", {})

@csrf_exempt
def offers(request):
    return render(request, "offers.html", {})

@csrf_exempt
def terms_and_conditions(request):
    return render(request, "terms_and_conditions.html", {})

# @csrf_exempt
# def faq(request):
#     return render(request, "faq.html", {})

@api_view(["POST"])
def login(request, format=None):
    try:
        print request.data, type(request.data)
        print "Captcha: ", request.POST.get("g-recaptcha-response")
        print request.POST
        data_str = ""

        for k,v in request.data.iteritems():
            data_str = k

        if data_str == "":
            print "You forgot to provide informations(In Web View)"
            return Response({"message": "You forgot to provide informations", "status": 400}, status=400)

        print k, type(k)
        data = json.loads(k)
        keys = ["contact", "password"]

        for key in keys:
            if not key in data:
                print "All information are required(contact, password)"
                return Response({"message": "All information are required(contact, password)", "status": 400}, status=400)

        
        print "Converting string data to an object"
        print "DATA => ", data

        password = data["password"]
        contact = data["contact"]
        

        # faq = Faq(questioned_by=User(id=1), question=question)
        try:
            user = User.objects.get(password=password, contact=contact)
        except User.DoesNotExist:
            return Response({"status":400 ,  "message": "Your credentials are invalid"}, status=400)

        print "Your credentials got successfully validated"
        return Response({"message": "Your credentials got successfully validated", "status": 200}, status=200)
    except Exception as e:
        print "Something unexpected occured on server side, ", e
        return Response({"status":400 ,  "message": "Something unexpected occured on server side"}, status=400)


def logout(request):
    return render(request, "logout.html", {})


    
