# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from Login.models import User
import validators

class Comment(models.Model):
	commented_by = models.ForeignKey(User)
	comment = models.TextField(null=False, blank=False, validators=[validators.validate_comment])
	commented_at = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __str__(self):
		return "Comment %d by User Id %d"%(self.id, self.commented_by.id)

	class Meta:
		verbose_name = "All Comment"	#s will be appended
		# verbose_name_plural = "Comments by users"
