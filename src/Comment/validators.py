from django.core.exceptions import ValidationError

def validate_comment(comment):
	if len(comment.strip()) < 10:
		raise ValidationError("Comment should have atleast 10 characters")


