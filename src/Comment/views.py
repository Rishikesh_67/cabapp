# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from models import Comment 

from django.views.decorators.csrf import csrf_exempt

@csrf_exempt
def get_comments(request):
	comments = Comment.objects.all()
	print comments
	return render(request, "comments.html", {"comments": comments})
