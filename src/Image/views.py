# -*- coding: utf-8 -*-
from __future__ import unicode_literals  

from django.shortcuts import render
# from .models import FishImage 
# from .serializers import FishImageSerializer
# from rest_framework.decorators import api_view
# from rest_framework.response import Response
# from rest_framework.views import APIView
from django.views.decorators.csrf import csrf_exempt

# custom error pages
# from django.shortcuts import render_to_response
# from django.template import RequestContext

# class FishImageAPIView(APIView):
#     """
#         List all fish infos
#     """
#     def get(self, request, format=None):
#         # snippets = Snippet.objects.all()
#         users = User.objects.all()
#         serializer = FishImageSerializer(users, many=True)
#         return Response(serializer.data)

@csrf_exempt
def get_images(request, slug):
	print slug
	return render(request, "images.html", {})

# @api_view(["GET"])
# def get_store_images(request, format=None):
# 	print "A Request for moving images"
# 	image_urls = [
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/room1.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/room2.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/room3.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/room4.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/shop.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/shop2.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/fish_and_meat_way.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/fishes.jpeg', 
# 		'https://gitlab.com/Rishikesh_67/images/tree/master/clients/fish_and_meat_way/front.jpeg'
# 	]

# 	return Response(image_urls, status=200)

# Handling errors and displaying custom error pages
# def handler404(request):
# 	response = render_to_response("404.html", {}, context_instance=RequestContext(request))
# 	response.status_code = 400
# 	return response