# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions

# def validate_discount(value):
# 	if value < 0.0:
# 		raise ValidationError("discount should be positive")

def validate_vehicle_pic(pic):
	w, h = get_image_dimensions(pic)

	if w < 600:
		raise ValidationError("Image width should be more than 600px, your is " + str(w) + "px")
	if h < 500:
		raise ValidationError("Image height should be more than 500px, your is " + str(h) + "px")
	
	print "Uploaded image's Width: ", w, " and Height: ", h

# class FishImage(models.Model):
# 	""" A class that defines the structure of User object """
# 	COLOR_CHOICES = (
# 	    (1, 'Type1'),
# 	    (2, 'Type2'),
# 	    (3, 'Type3'),
# 	    (4, 'Type4'),
# 	    (5, 'Type5'),
# 	    (6, 'Type6'),
# 	    (7, "Other"),
# 	)

# 	flesh_name = models.CharField(max_length=50, blank=False)	#Required
# 	flesh_type = models.CharField(max_length=50, blank=False)
# 	flesh_pic = models.ImageField(default="https://en.wikipedia.org/wiki/List_of_types_of_seafood#/media/File:Squilla_mantis_(l%27Ametlla)_brighter_and_quality.jpg", max_length=200)
# 	flesh_type = models.IntegerField(choices=COLOR_CHOICES, default='Type0', blank=True)
# 	price = models.FloatField(blank=False)
# 	discount = models.IntegerField(validators=[validate_discount], default=0, blank=True)

# 	def __unicode__(self):
# 		return self.flesh_name

class HomeMovingVehicleImage(models.Model):
	title = models.CharField(max_length=12, null=False, blank=False)
	description = models.TextField(max_length=60, blank=False, null=False)
	vehicle_pic = models.ImageField(max_length=200, null=False, blank=False, validators=[validate_vehicle_pic])
	
	def __unicode__(self):
		return self.title + " - " + self.description[0:10] + "..."  
		


		


 