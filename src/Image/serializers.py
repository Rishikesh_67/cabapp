from rest_framework import serializers
from .models import FishImage

class FishImageSerializer(serializers.HyperlinkedModelSerializer):
	""" A serializer class of user model """
	class Meta:
		model = FishImage
		fields = "__all__" 
							
