# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-08-12 13:58
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Register', '0003_auto_20170812_1910'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='email_matching_link',
        ),
    ]
