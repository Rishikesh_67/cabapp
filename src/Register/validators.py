import re
from django.core.exceptions import ValidationError

def validate_contact(v):
	print "Validating contact number ", v, "of type ",type(v)
	if not re.match(r"^([789])([0-9]{9})$", str(v)):
		raise ValidationError("Contact number should only contain 10 digits starting with 7/8/9")