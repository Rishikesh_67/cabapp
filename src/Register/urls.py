from django.conf.urls import  url
import views
import conf

try: 
	urlpatterns = [
	    url(r"^$", views.register, name="register"),
	    url(r"^submit/$", views.store_registration_details, name="store-registration-details"),
	    url(r"^confirm-account/"+conf.EMAIL_TEXT+"/(?P<email_md5>.*)/(?P<email>.*)/", views.confirm_account, name="confirm-account"),
	    url(r"^error/$", views.error, name="error"),
	    # Once account get confimed then user will be redirected to this particular url
	    url(r"^account-confirmed/(?P<email>.*)/"+conf.EMAIL_TEXT+"/(?P<email_md5>.*)/", views.account_confirmed, name="account-confirmed"),
	]
except:
	print "Error Ocurred in Register/urls.py"
