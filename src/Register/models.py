# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
import validators
from django.utils import timezone
import datetime

class User(models.Model):
	""" A class that defines the structure of User object """
	fname = models.CharField(max_length=50, default="", blank=False)	#Required
	# lname = models.CharField(max_length=50, default="", blank=False)	#Required
	contact = models.PositiveIntegerField(validators= [validators.validate_contact] ,blank=False, null=False, unique=True)	#Required 
	email = models.EmailField(max_length=255, blank=True) #Optional
	password = models.CharField(max_length=20, default="", blank=True)
	profile_pic = models.ImageField(default="user.png", max_length=200)
	is_active = models.BooleanField(default=True)
	is_admin = models.BooleanField(default=False)
	app_key = models.TextField(default="", blank=True)
	email_confirmed = models.BooleanField(default=False)
	advance_paid = models.PositiveIntegerField(default=0)
	# email_matching_link = models.TextField(default="")
	otp = models.IntegerField(default=0, blank=True)
	otp_created_at = models.DateTimeField(default=None, blank=True, null=True)	#default=timezone.now
	created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __unicode__(self):
		# +91 73537 87704
		# print self.contact, type(self.contact), str(self.contact), type(str(self.contact))
		# if self.fname != "":
			# full_name = self.fname + " " + self.lname
			# return "%100s %s %12d"%(full_name, " "*3, self.contact)
			# return str(self.id)+ " - " +full_name + ",   " + "+91 " + str(self.contact)[:5] + " " + str(self.contact)[5:]
		# return "%100s %s %12d"%("---", " "*3, self.contact)
		return "User_" + str(self.id)+ ":  " + "+91 " + str(self.contact) 
 