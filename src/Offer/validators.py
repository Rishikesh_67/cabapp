from django.core.exceptions import ValidationError
from django.core.exceptions import ValidationError
from django.core.files.images import get_image_dimensions

def validate_offer_message(offer_message):
	if len(offer_message.strip()) < 5:
		raise ValidationError("Offer message should have atleast 5 characters")
        
def validate_image(image):
	print "Got image as ", image
	w, h = get_image_dimensions(image)
	print "Uploading an image with width: ", w, " and height: ",h

	if w < 700:
		raise ValidationError("Image width should be more than 700px, your is "+str(w)+"px")
	if h < 500:
		raise ValidationError("Image height should be more than 500px, your is "+str(h)+"px")
	
	print "Uploaded image's Width: ", w, " and Height: ", h




