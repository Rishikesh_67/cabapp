# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import validators 

class Offer(models.Model):
	title = models.CharField(max_length=500, blank=False, null=False)
	image = models.ImageField(validators=[validators.validate_image])
	offer_message = models.TextField(validators=[validators.validate_offer_message])
	created_at = models.DateTimeField(auto_now_add=True, auto_now=False)
	updated_at = models.DateTimeField(auto_now_add=False, auto_now=True)

	def __str__(self):
		return "Offer %d | %s"%(self.id, self.title)

	class Meta:
		verbose_name = "Online Offer"