# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from .models import Offer 

@csrf_exempt
def get_offers(request):
	print "Displaying offers"
	offers = Offer.objects.all()
	return render(request, "offers.html", {"offers": offers})

