# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from django.views.decorators.csrf import csrf_exempt
from rest_framework.response import Response
from rest_framework.decorators import api_view
import json
from models import Contact
from django.core.mail import send_mail
import conf 

@csrf_exempt
def contact(request):
	return render(request, "contact.html", {})
	
def mailer(request):
	print request.data
	# data = request.data

	print "request.POST", request.POST
	print type(request.POST),"\n"
	# data = request.data.dict().keys()[0]
	# print data
	print "request.POST.dict() = ", request.POST.dict()
	print type(request.POST.dict()), "\n"

	print "request.POST.dict().keys() = ", request.POST.dict().keys()
	print type(request.POST.dict().keys()), "\n"

	print "request.POST.dict().keys()[0] = ", request.POST.dict().keys()[0]
	print type(request.POST.dict().keys()[0]), "\n"
	data = {}
	# mailer() is using data so data is global here
	data.update(json.loads(request.POST.dict().keys()[0]))	
	print "json.loads(request.POST.dict().keys()[0]): ", data
	print type(data), "\n"

	for key, value in data.iteritems():
		print key, value

	Contact.objects.create(**data)

	send_mail(
		    'CONTACT MAIL FROM '+data["fullname"].strip(),	# header
		    "",	# message
		    'surecabstoursandtravels@gmail.com',
		    [data["email"]],
		    fail_silently=True,
		    html_message="<table style='border-collapse:collapse; '>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn1.iconfinder.com/data/icons/business-finance-vol-10-2/512/21-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Fullname</b></td>"+
		    	"<td style='padding:5px'>"+data["fullname"]+"</td>"
		    "</tr>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn1.iconfinder.com/data/icons/mail-contact-and-subscription/249/mail-4-r-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Email</b></td>"+
		    	"<td style='padding:5px'>"+data["email"]+"</td>"
		    "</tr>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn4.iconfinder.com/data/icons/medical-flat-outline-2/614/485_-_Chat-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Contact</b></td>"+
		    	"<td style='padding:5px'>"+data["contact"]+"</td>"
		    "</tr>"+
		     "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn0.iconfinder.com/data/icons/twitter-ui-flat/48/Twitter_UI-18-32.png'/></td>"+
		    	"<td style='padding:5px'><b>Message</b></td>"+
		    	"<td style='padding:5px'>"+data["message"]+"</td>"
		    "</tr>"+
		    "</table>"
	)
	print "Successfully sent the mail"

# A function that sends contact data 
@csrf_exempt
@api_view(["POST"])
def send_contact_message(request, format=None):
	try:
		import threading 
		
		threading.Thread(target=mailer, args=[request]).start()

		print "Created a new contact details"

		return Response({"status": 200, "message": "Contact details successfully sent"}, status=200)
	except Exception as e:
		print "Error occured while", e
		return Response({"status": 400, "message": "Something unexpected happened on server side"}, status=400)


