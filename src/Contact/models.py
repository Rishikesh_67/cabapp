# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
import validators
from django.utils import timezone

class Contact(models.Model):
	fullname = models.CharField(max_length=50, null=False, blank=False)
	email = models.EmailField(null=False, blank=False)
	contact = models.CharField(null=False, blank=False, validators=[validators.validate_contact], max_length=10)
	message = models.TextField(blank=True, null=False)
	created_at = models.DateTimeField(auto_now=True, auto_now_add=False)

	def __str__(self):
		return "At %s | %d by %s - %s"%( str(self.created_at)[:19], self.id, self.fullname, self.email)

	class Meta:
		verbose_name_plural = "People who contacted us"
