from django.conf.urls import include, url

from Book import views

urlpatterns = [
	# Show my shipping and booking details
    url(r"^my-booking-and-shipping-details/$", views.my_booking_and_shipping_details, name="my-booking-and-shipping-details"),
    
    # REST API END POINT TO FETCH BOOKING DETAILS
    url(r"^get-my-booking-and-shipping-details/(?P<booking_id>\w+)/$", views.get_my_booking_and_shipping_details_api, name="get-my-booking-and-shipping-details"),

    url(r"^booking-and-shipping-details/(?P<booking_id>\w+)/$", views.booking_and_shipping_details, name="get-my-booking-and-shipping-details"),

    url(r"^booking-details/$", views.get_booking_details, name="get-booking-details"),

    url(r"^all-booking-details/$", views.get_all_booking_details, name="all-booking-details"),

    url(r"^book-now/$", views.book_now, name="book-now"),
    # url(r"^vehicles-list/(?P<members>[-\w]+)/(?P<dot>^[0-9]{4}-[0-9]{2}-[0-9]{2}$)/(?P<dor>^[0-9]{4}-[0-9]{2}-[0-9]{2}$)", views.vehicles_list, name="vehicles-list")
   	url(r"^vehicles-list/(?P<members>[-\w]+)/(?P<dot>[-\w]+)/(?P<dor>[-\w]+)/", views.vehicles_list, name="vehicles-list"),

   	url(r"booking-summary/(?P<vehicle_id>\d+)/", views.booking_summary, name="booking_summary"),
    
    url(r"local-booking/", views.local_booking, name="local_booking"),

    url(r"local-booking-details/submit/", views.get_local_booking_details, name="local-booking-details"),

    url(r"fill-booking-confirmation-details/", views.fill_booking_confirmation_details, name="fill-booking-confirmation-details"),
    
    url(r"^send-otp/$", views.send_otp, name="send-otp"),
    
    url(r"^store-booking-details/", views.store_booking_details, name="store-booking-details"),
    
    url(r"^success/$", views.success, name="success"),

    url(r"^error/$", views.error, name="error"),

    url(r"^sure-cabs-vehicle-booking-details/(?P<booking_id>[-\w:]+)/Surehaveersh_sandar5rarjshw67andHY67GULL_offline/(?P<booking_id_md5>[-\w]+)/$", views.show_booking_details,name="show-booking-details"),

    url(r"^local-booking-details-messenger/$", views.local_booking_details_messenger, name="local-booking-details-messenger"),
]
