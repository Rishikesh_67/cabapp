# -*- coding: utf-8 -*-
from __future__ import unicode_literals  

from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import BookingDetail, Vehicle
from django.conf import settings
from django.shortcuts import render, redirect
import json
import requests
from datetime import date, time, datetime
from Register.models import User
from .models import OurVehicle, BookingNew
from django.db.models import Q
from django.core.mail import send_mail
from django.views.decorators.csrf import csrf_exempt
from .models import BookingDetailStore
import hashlib
# from django.core.serializers import serialize

# from django.utils.encoding import force_text
# from django.core.serializers.json import DjangoJSONEncoder
# import json
# import sys  
# reload(sys)  
# sys.setdefaultencoding('utf8')

# class LazyEncoder(DjangoJSONEncoder):
#     def default(self, obj):
#         if isinstance(obj, ShoppingDetail):
#             return force_text(obj)
#         return super(LazyEncoder, self).default(obj)

@api_view(["GET"])
def get_booking_details(request, format=None):
	# serialized = serialize('json', ShoppingDetail.objects.all(), cls=LazyEncoder)
	# print serialized, type(serialized)	
	# return Response(json.loads(serialized), status=200)

	booking_details = BookingDetail.objects.all()
	print "Booking details [",len(booking_details), booking_details

	booking_details_arr = []

	for booking_detail in booking_details.iterator():
		print "Creating record for this booking"
		booking_detail_dict = {}
		customer = booking_detail.customer

		
		booked_vehicles =  booking_detail.vehicles.all()
		# booked_vehicles = []
		print "Got ", len(booked_vehicles)

		if not booking_detail.is_paid:
			continue

		vehicles = []
		total_price = 0.0
		for vehicle in booked_vehicles:
			vehicle_dict = {}
			vehicle_dict["id"] = vehicle.id
			vehicle_dict["name"] = vehicle.name
			vehicle_dict["image"] = vehicle.image.url
			price = vehicle.price
			vehicle_dict["price"] = price
			# product_dict["discount"] = product.discount
			vehicle_dict["description"] = vehicle.description[:120]+"..."
			# product_dict["items_count"] = product.items_count
			total_price += price
			vehicles.append(vehicle_dict)
			print "=>", request.get_host()
			print "=>", request.get_full_path()
			
			print settings.MEDIA_ROOT + vehicle_dict["image"].replace(settings.MEDIA_URL, "/")
			# print settings.MEDIA_URL + product_dict["image"]
		print "Product list prepared"

		booking_detail_dict["customer"] = {	
											"user_id": customer.id, "email": customer.email, 
											"contact": customer.contact, 
											"full_name": customer.fname + " " + customer.lname,
											"profile_pic": customer.profile_pic.url,
											"address": customer.address
										}

		booking_detail_dict["booked_vehicles"] = vehicles
		booking_detail_dict["date_of_travel"] = booking_detail.date_of_travel
		booking_detail_dict["date_of_return"] = booking_detail.date_of_return
		booking_detail_dict["pick_up_time"] = booking_detail.pick_up_time
		booking_detail_dict["is_paid"] = booking_detail.is_paid
		booking_detail_dict["total_price"] = total_price
		booking_details_arr.append(booking_detail_dict)

		print booking_details_arr

	return Response(booking_details_arr, status=200)


def get_all_booking_details(request):
	# For Admin
	if request.user.is_authenticated():
		return render(request, "all-booking-details.html", {})
	else:
		return redirect("/admin/")


def my_booking_and_shipping_details(request):

	return render(request, "my_booking_and_shipping_details.html", {})


@api_view(["POST"])
def book_now(request, format=None):
	try:
		print request.data, type(request.data)
		data_str = ""

		for k,v in request.data.iteritems():
			data_str = k

		if data_str == "":
			print "You forgot to provide informations(In Web View)"
			return Response({"message": "You forgot to provide informations", "status": 400}, status=400)

		print k, type(k)
		data = json.loads(k)
		keys = ["from", "to", "dor", "dot", "members", "hours", "minutes", "periods", "contact"]
		for key in keys:
			print key
			if not key in data:
				print key
				print "All information are required(from, to, dot, dor)"
				return Response({"message": "All information are required(from, to, dot, dor, members, hour, minute, period)", "status": 400}, status=400)

		
		print "Converting string data to an object"
		print "DATA => ", data

		frm = data["from"]
		to = data["to"]
		dot = data["dot"]
		dor = data["dor"]
		# hour = data["hours"]
		# minute = data["minutes"]

		hour = "0"
		if data["hours"][0] == "0":
			hour = data["hours"][1]
		else:
			hour = data["hours"]
		print "HOUR ", hour
		
		minute = "0"
		if data["minutes"][0] == "0":
			minute = data["minutes"][1]
		else:
			minute = data["minutes"]

		print "MINUTE ", minute

		period = data["periods"]

		arr = dot.split("/")
		dot_str = "-".join([arr[2], arr[0], arr[1]])
		print "DOT - ", dot_str

		year1 = int(arr[2])
		month1 = int(arr[0])
		day1 = int(arr[1])

		arr = dor.split("/")
		dor_str = "-".join([arr[2], arr[0], arr[1]])
		print "DOR - ", dor_str

		year2 = int(arr[2])
		month2 = int(arr[0])
		day2 = int(arr[1])

		user = User.objects.get(contact=data["contact"])
		print user
		print "User got selected"

		vehicle = OurVehicle.objects.get(id=1)
		# vehicle2 = Vehicle.objects.get(id=7)

		booking_arr = str(datetime.now()).split()
		print booking_arr
		booking_id = booking_arr[1].split(":")[2].split(".")[1]	


		new_booking = BookingNew(
						booking_id="SURECABS"+booking_id,
						customer=user, 
						source=frm, 
						destination=to, 
						# vehicles=[Vehicle.objects.get(id=3)],
						vehicles = OurVehicle.objects.get(id=1),
						date_of_return=date(year2, month2, day2),  
						date_of_travel=date(year1, month1, day1), 
						pick_up_time=time(int(hour), int(minute), 0),
						is_paid=True,
					)

		new_booking.save()
		# new_booking.vehicles.add(vehicle)	
		send_mail('BOOKING INFO',	# header
		    "",	# message
		    'surecabstoursandtravels@gmail.com',
		    [ user.email ],
		    fail_silently=True,
		    html_message="Click here <a href='"+
		    "https://surecabs.in/v1/booking/booking-and-shipping-details/"+str(new_booking.booking_id)+"/'>to see the booking details </a>"
		   )

		print "Successfully booked the ride"

		return Response({"status": 200, "name": "Rishikesh", "message": "You successfully booked your ride", "booking_id": new_booking.booking_id}, status=200)
	except Exception as e:
		print "Something unexpected occured on server side, ", e
		return Response({"status": 400 , "name": "Rishikesh", "message": "Something unexpected occured on server side"}, status=400)


def vehicles_list(request, members, dot, dor):
	print "Selected number of members", members, dot, dor
	try:
		dot_arr = dot.split("-")
		dor_arr = dor.split("-")
		# 08-25-2017/08-30-2017/
		d1 = date(int(dot_arr[2]), int(dot_arr[0]), int(dot_arr[1]))
		d2 = date(int(dor_arr[2]), int(dor_arr[0]), int(dor_arr[1]))

		days = (d2 - d1).days + 1
		print "Booking for {0} days".format(days)
		if not (days >= 1):
			return render(request, "unexpected.html", {})
		elif days > 62:
			return render(request, "not-allowed.html", {})
	except Exception as e:
		print e
		return render(request, "unexpected.html", {})
	# category = ""
	# max_passengers = 0

	vehicles = []
	# if members == "1-4":
	# 	# category = "CAB/TAXI"
	# 	category = "cab/taxi"
	# 	max_passengers = 4
	# elif members == "5-7":
	# 	# category = "INNOVA"
	# 	category = "innova"
	# 	max_passengers = 6
	# elif members == "8-13":
	# 	max_passengers = 8
	# 	# category = "TEMPO TRAVELLER"
	# 	category = "tempo-traveller"
	# elif members == "14-21":
	# 	# category = "MINI BUS"
	# 	category = "mini-bus"
	# elif members == "22-33" or members == "34-52":
	# 	# category = "BUS"
	# 	category = "bus"
	# else:
	# 	category = "cab/taxi"

	# print "You want to book vehicles of category ", category
	vehicles_cabs = OurVehicle.objects.filter(category="cab/taxi")
	vehicles_innova = OurVehicle.objects.filter(category="innova")
	vehicles_tempo_traveller = OurVehicle.objects.filter(category="tempo-traveller")
	vehicles_mini_bus = OurVehicle.objects.filter(category="mini-bus")
	vehicles_bus = OurVehicle.objects.filter(category="bus")

	vehicles.extend(vehicles_cabs)
	vehicles.extend(vehicles_innova)
	vehicles.extend(vehicles_tempo_traveller)
	vehicles.extend(vehicles_mini_bus)
	vehicles.extend(vehicles_bus)

	# costs = []
	# for vehicle in vehicles:
	# 	costs.append(vehicle.per_km_cost_for_non_ac*)

	# vehicles = vehicles_cabs + vehicles_innova + vehicles_tempo_traveller + vehicles_mini_bus + vehicles_bus
	# vehicles = OurVehicle.objects.all()
	# Q(creator=owner) | Q(moderated=False)
	# vehicles = OurVehicle.objects.filter(Q(category=category) | Q( max_passengers=max_passengers))

	# print "Got ", len(vehicles), "vehicles of category (", category, ")"

	print vehicles
	return render(request, "vehicles_list.html", {"vehicles": vehicles, "days": days, "dot": dot, "dor": dor})

@api_view(["POST"])
def get_my_booking_and_shipping_details_api(request, booking_id,format=None):
	try:
		data = {}
		print "You requested booking and shipping details for ", booking_id
		print request.data
		data["status"] = 200
		data["message"] = "Operation successful"

		try:
			print "Fetching detials for booking id ", booking_id
			new_booking_detail = BookingNew.objects.get(booking_id=booking_id)
			print new_booking_detail
			data["booking_id"] = new_booking_detail.booking_id
			data["selected_vehicle"] = new_booking_detail.vehicles.name
			data["source_city"] = new_booking_detail.source
			data["destination_city"] = new_booking_detail.destination
			data["pick_up_day"] = new_booking_detail.date_of_travel
			data["selected_vehicle"] = new_booking_detail.vehicles.category
			data["passengers"] = new_booking_detail.vehicles.max_passengers
			data["return_day"] =  new_booking_detail.date_of_return

			if new_booking_detail.vehicles.ac_and_non_ac_facility:
				data["price_per_km"] = new_booking_detail.vehicles.per_km_cost_for_ac
			else:
				data["price_per_km"] = new_booking_detail.vehicles.per_km_cost_for_non_ac

			data["min_km_per_day"] = new_booking_detail.vehicles.minimum_km_per_day
			data["driver_bata"] = new_booking_detail.vehicles.driver_bata

			# print str(data["pick_up_day"]),"hghg"
			data["duration"] =  str(int(str(data["return_day"]).split("-")[2]) - int(str(data["pick_up_day"]).split("-")[2]) + 1) 
 			data["estimated_fare"] = data["price_per_km"]*int(data["duration"])*data["min_km_per_day"]
 			data["advance_paid"] = new_booking_detail.customer.advance_paid
 			data['discount'] = new_booking_detail.vehicles.discount

 			print data
			print "Successfully fetched the data"
		except Exception as e:
			print "This BOOKING ID" + booking_id +" does not exist"
			print e
			return Response({"message": "Your requested booking id does not exist", "status":400}, status=400)

		return Response(data, status=200)
	except:
		print "Some error occured on server side while fetching shipping and booking details"
		return Response({"status": 500, "message": "Some error occured on server side"}, status=400)

def booking_and_shipping_details(request, booking_id):
	print booking_id
	return render(request, "booking_and_shipping_details.html", {"booking_id": booking_id})

@csrf_exempt
def booking_summary(request, vehicle_id):
	# print "Selected number of members", members, dot, dor
	# try:
	# 	dot_arr = dot.split("-")
	# 	dor_arr = dor.split("-")
	# 	# 08-25-2017/08-30-2017/
	# 	d1 = date(int(dot_arr[2]), int(dot_arr[0]), int(dot_arr[1]))
	# 	d2 = date(int(dor_arr[2]), int(dor_arr[0]), int(dor_arr[1]))

	# 	days = (d2 - d1).days + 1
	# 	print "Booking for {0} days".format(days)
	# 	if not (days >= 1):
	# 		return render(request, "unexpected.html", {})
	# except Exception as e:
	# 	print e
	# 	return render(request, "unexpected.html", {})

	try:
		print vehicle_id
		vehicle = vehicles=OurVehicle.objects.get(pk=int(vehicle_id))
		print vehicle
	except Exception as e:
		print e
		return render(request, "unexpected.html", {})
	return render(request, "booking_summary.html", {"vehicle": vehicle})

@csrf_exempt
def local_booking(request):
    minutes = []
    for i in range(0, 60):
        if len(str(i)) == 1:
            minutes.append("0"+str(i))
        else:
            minutes.append(str(i))

    print minutes

    hours = []
    for i in range(1,13):
        if len(str(i)) == 1:
            hours.append("0"+str(i))
        else:
            hours.append(str(i))

    vehicles = OurVehicle.objects.all()
    # vehicles.exclude(pk=0)
    # vehicles.pop(1)
    # vehicles.pop(2)
    # vehicles.pop(7)

    return render(request, "local_booking.html", {"hours": hours, "minutes": minutes, "vehicles": vehicles})

def fill_booking_confirmation_details(request):
	return render(request, "fill_booking_confirmation_details.html", {})

def thread_message_mail_sender(data, booking_id):
	print "Thread", data, 
	# import is here to stop unnecessary imports
	
	# url = "http://control.mavyah.com/vendorsms/pushsms.aspx?user=offline&password=offline@123&msisdn="+ data["contact"].strip()+"&sid=SURCAB&msg="+msg+"&fl=0&gwid=2"
	# print url
	# response = requests.get(url)
	# response = requests.get("http://control.mavyah.com/vendorsms/pushsms.aspx?user=offline&password=offline@123&msisdn="+ data["contact"].strip()+"&sid=SURCAB&msg=Dear Customer.Thanks for booking cab. Your cab will be arriving shortly.&fl=0&gwid=2")
	
	# mob_num = "9590331111"	#MAM1	
	# mob_num = "9036363888"	#MAM2
	# mob_num = "8553859952" 	#SIR
	mob_num = "7353787704" 	#RISHIKESH
	try:
		# msg = "Name"+data["fname"]+", Phone number"+ data["contact"]+", Email id "+data["email"]+", Location"+data["vehicle"]+",Pickup time-"+data["pickup_time"]

		# NEW MSG TEMPLATE
		rough_text = "Surehaveersh_sandar5rarjshw67andHY67GULL_offline"
		msg = "Name-%s, Ph-%s, Pickup time-%s/%s, Vehicle-%s"%(data["fname"], data["contact"], data["dot"], data["pickup_time"], data["vehicle"])
		print "MESSAGE - ", msg
		url2 = "http://control.mavyah.com/vendorsms/pushsms.aspx?user=offline&password=offline@123&msisdn=%s&sid=SURCAB&msg=%s&fl=0&gwid=2"%(mob_num, msg)
		response2 = requests.get(url2)
		print response2.status_code
		print response2.text 
		print "SMS SENT TO OWNER"

		print "Sending Mail Now"
		print "SENDING mail to ", data["email"]

		send_mail(
			    'YOUR BOOKING REQUEST DETAIL',	# header
			    "",	# message
			    'surecabstoursandtravels@gmail.com',
			    [data["email"]],
			    fail_silently = True,
			    html_message = "Name - <b>" + data["fname"] + "</b><br>"+
			    "Contact Sure Cabs- <b>" +"959033111" + "</b><br>"+
			    "Email - <b>" + data["email"] + "</b><br>"+
			    "Pick up location - <b>" + data["frm"] + "</b><br>"+
			    "Pick up date - <b>"+ data["dot"] + "</b><br>"+
			    "Duration - for <b>"+ data["total_days"] + "</b> days<br>"+
			    "Pick time - <b>"+ data["pickup_time"] + "</b><br>"+
			    "Vehicle name - <b>"+ data["vehicle"] + "</b><br>"+
			    "Destination - <b>"+ data["to"] + "</b><br>"+
			    "Total cost - <b>" + str(int(data["cost1"]) + int(data["cost2"])) + "</b><br>" +
			    "You can check your booking details  <a href='"+"https://www.surecabs.in/v1/booking/sure-cabs-vehicle-booking-details/"+booking_id+"/"+rough_text+"/"+hashlib.md5(booking_id).hexdigest()+"/'> click here</a><br>"
		)
		print "MAIL SENT"
	except requests.exceptions.ConnectionError as e:
		print "Check your NETWORK connection"
		print e
		return Response({"message": "NETWORK CONNECTION error, we will come back soon",  "status": 400}, status=400)
	except Exception as e:
		print "Something unexpected happened on server side"
		print e
		return Response({"message": "Something unexpected happened on server side",  "status": 400}, status=400)
		
def save_data(data, booking_id):
	import random
	
	try:
		newbook = BookingDetailStore.objects.create(booking_id=booking_id, **data)
		newbook.save()
		print "Booking details successfully saved"
	except KeyError as e:
		print "Key error", e
	except Exception as e:
		print "Exception ", e, " let me find another randome NUMBER"


@csrf_exempt
@api_view(["POST"])
def store_booking_details(request, format=None):
	try:
		data = {}
		# print "DATA", request.data
		data.update(json.loads(request.POST.dict().keys()[0]))
		print "DATA: ", data
		print "SENDING SMS TO ", data['contact']

		""" THREAD TO SEND DETAILS """
		print "Starting thread to send mail and message"
		
		# Local import
		import threading
		from datetime import datetime
		s = str(datetime.now())
		lst = s.split()
		booking_id = "SURECABS-" 
		booking_id += lst[0]+lst[1].split(".")[0]+lst[1].split(".")[1]

		threading.Thread(target=thread_message_mail_sender, args=[data, booking_id]).start()
		print "Successfully SENT the message and mail"

		""" SAVE DETAILS ON DB """
		threading.Thread(target=save_data, args=[data, booking_id]).start()

		return Response({"message": "OTP SENT TO YOUR MOBILE NUMBER",  "status": 200}, status=200)
	except requests.exceptions.ConnectionError as e:
		print "Check your NETWORK connection"
		print e
		return Response({"message": "NETWORK CONNECTION error, we will come back soon",  "status": 400}, status=400)
	except Exception as e:
		print "Something unexpected happened on server side"
		return Response({"message": "Something unexpected happened on server side",  "status": 400}, status=400)
		print e

""" SUCCESS MESSAGE """
def success(request):
	return render(request, "success.html", {})

def otp_sender_thread(data, otp):
	# working one
	# url = "http://control.mavyah.com/vendorsms/pushsms.aspx?user=offline&password=offline@123&msisdn=%s&sid=SURCAB&msg=Dear Customer, Welcome to SureCabs. Your One Time Password(OTP) is %s.&fl=0&gwid=2"%(data["contact"].strip(), otp)
	url = "http://control.mavyah.com/vendorsms/pushsms.aspx?user=offlineotp&password=Mavyah@12345&msisdn=%s&sid=SURCAB&msg=Dear Customer, Welcome to SureCabs. Your One Time Password(OTP) is %s.&fl=0&gwid=2"%(data["contact"].strip(), otp)
	# response = requests.get("http://control.mavyah.com/vendorsms/pushsms.aspx?user=offline&password=offline@123&msisdn="+ data["contact"].strip()+"&sid=SURCAB&msg=Dear Customer.Thanks for booking cab. Your cab will be arriving shortly.&fl=0&gwid=2")
	print "Using " + url + " for sending OTP"
	response = requests.get(url)
	print response.status_code
	print response.text

@csrf_exempt
@api_view(["POST"])
def send_otp(request, format=None):
	try:
		data = {}
		# print "DATA", request.data
		data.update(json.loads(request.POST.dict().keys()[0]))
		print data

		# Otherwise Broken Pipe
		print "SENDING OTP"
		import random
		s = str(random.random())
		otp = s[len(s)-5:len(s)-1] 

		import threading
		threading.Thread(target=otp_sender_thread, args=[data, otp]).start()
		print "OTP SENT"

		return Response({"otp": otp, "message": "OTP SENT","status":200}, status =200)
	except requests.exceptions.ConnectionError  as e:
		print "Check your NETWORK connection"
		print e
		return Response({"message": "NETWORK CONNECTION error, we will come back soon",  "status": 400}, status=400)
	except Exception as e:
		print "Something unexpected happened on server side"
		print e
		return Response({"message": "Something unexpected happened on server side",  "status": 400}, status=400)
		
# data = {}

def local_booking_mailer(data):
	send_mail(
		    'SURECABS - LOCAL VEHICLE BOOKING DETAILS',	# header
		    "",	# message
		    'surecabstoursandtravels@gmail.com',
		    [data["email"]],
		    fail_silently=True,
		    html_message="<table style='border-collapse:collapse;'>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn1.iconfinder.com/data/icons/business-finance-vol-10-2/512/21-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Fullname</b></td>"+
		    	"<td style='padding:5px'>"+data["fullname"]+"</td>"
		    "</tr>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn1.iconfinder.com/data/icons/mail-contact-and-subscription/249/mail-4-r-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Email</b></td>"+
		    	"<td style='padding:5px'>"+data["email"]+"</td>"
		    "</tr>"+
		    "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn4.iconfinder.com/data/icons/medical-flat-outline-2/614/485_-_Chat-24.png'/></td>"+
		    	"<td style='padding:5px'><b>Contact</b></td>"+
		    	"<td style='padding:5px'>"+data["contact"]+"</td>"
		    "</tr>"+
		     "<tr>"+
		    	"<td style='padding:5px'><img src='https://cdn0.iconfinder.com/data/icons/twitter-ui-flat/48/Twitter_UI-18-32.png'/></td>"+
		    	"<td style='padding:5px'><b>Selected Vehicle</b></td>"+
		    	"<td style='padding:5px'>"+data["vehicle"]+"</td>"
		    "</tr>"+
		    "</table><br><b>We will get back to you soon.<br>Thank you so much for your mail.</b>"
		   
	)
	print "\nSuccessfully sent the mail"


# A function that sends local booking data 
@csrf_exempt
@api_view(["POST"])
def get_local_booking_details(request, format=None):
	data = {}
	try:
		print request.data
		print "request.POST", request.POST
		print type(request.POST),"\n"
		# data = request.data.dict().keys()[0]
		# print data
		print "request.POST.dict() = ", request.POST.dict()
		print type(request.POST.dict()), "\n"

		print "request.POST.dict().keys() = ", request.POST.dict().keys()
		print type(request.POST.dict().keys()), "\n"

		print "request.POST.dict().keys()[0] = ", request.POST.dict().keys()[0]
		print type(request.POST.dict().keys()[0]), "\n"

		# mailer() is using data so data is global here
		data.update(json.loads(request.POST.dict().keys()[0]))	
		print "json.loads(request.POST.dict().keys()[0]): ", data
		print type(data), "\n"

		for key, value in data.iteritems():
			print key, value

		import threading 
		
		# threading.Thread(target = local_booking_mailer, args=[data]).start()
		print "SENDING OTP"
		import random
		s = str(random.random())
		otp = s[len(s)-5:len(s)-1] 

		threading.Thread(target=otp_sender_thread, args=[data, otp]).start()
		print "OTP SENT"
		
		print "Responding JSON to client"

		return Response({"status": 200, "message": "Local booking details successfully sent", "otp": otp}, status=200)
	except Exception as e:
		print "Error occured while", e
		return Response({"status": 400, "message": "Something unexpected happened on server side"}, status=400)


def local_booking_msg_sender_to_owner_thread(data):
	print "A THREAD THAT SENDS SMS TO OWNER"
	# mob_num = "9590331111"	#MAM1	
	# mob_num = "9036363888"	#MAM2
	# mob_num = "8553859952" 	#SIR
	mob_num = "7353787704" 	#RISHIKESH
	try:
		# msg = "Name"+data["fname"]+", Phone number"+ data["contact"]+", Email id "+data["email"]+", Location"+data["vehicle"]+",Pickup time-"+data["pickup_time"]
		# NEW MSG TEMPLATE
		msg = "Name%s, Phone number%s, Email id %s, Location%s"%("-"+data["fullname"], "-"+data["contact"], "-"+data["email"], ", Vehicle-"+data["vehicle"])
		print "MESSAGE - ", msg
		url2 = "http://control.mavyah.com/vendorsms/pushsms.aspx?user=offline&password=offline@123&msisdn=%s&sid=SURCAB&msg=%s&fl=0&gwid=2"%(mob_num, msg)
		response2 = requests.get(url2)
		print response2.status_code
		print response2.text 
		print "SMS SENT TO OWNER"
		
	except requests.exceptions.ConnectionError as e:
		print "Check your NETWORK connection"
		print e
		# return Response({"message": "NETWORK CONNECTION error, we will come back soon",  "status": 400}, status=400)
	except Exception as e:
		print "Something unexpected happened on server side"
		print e
		# return Response({"message": "Something unexpected happened on server side",  "status": 400}, status=400)

	print "Successfully sent the message"


@csrf_exempt
@api_view(["POST"])
def local_booking_details_messenger(request, format=None):
	try:
		print "LOCAL BOOKING DETAILS"
		data = json.loads(request.POST.dict().keys()[0])
		print data 
		import threading
		print "ACTIVATING THREAD FOR SENDING MESSAGE"
		threading.Thread(target=local_booking_msg_sender_to_owner_thread, args=[data]).start()
		print "ACTIVATING THREAD FOR SENDING MAIL"
		threading.Thread(target=local_booking_mailer, args=[data]).start()
		print "ACTIVATED"
		return Response({"message": "Dear customer, thanks for booking", "status": 200}, status=200)
	except Exception as e:
		print e
		return Response({"message": "Something unexpected happened on server side", "status": 400}, status=400)

def error(request):
	return render(request, "error.html", {})

def show_booking_details(request, booking_id, booking_id_md5):
	if hashlib.md5(booking_id).hexdigest() != booking_id_md5:
		return render(request, "error.html",{"message": "You are trying to perform unwanted action"})

	try:
		booking = BookingDetailStore.objects.get(booking_id=booking_id)
		print "Successfully retrieved details from database"
	except:
		return render(request, "error.html",{"message": "It didn't seemed that you booking exists"})

	return render(request, "booking_details.html", {"booking": booking})
