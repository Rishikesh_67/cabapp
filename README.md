# cab-book

A repository containing the source of a Cab Booking app's REST APIs.

## API Blueprint

<!-- Check **API blue print** [here](./docs/api-blueprint.apib) -->

Check  **API blue print** [here](./docs/api-blueprint.mdown)

## References

Visit [here](./docs/pip-install-and-usage-guides.md) to see pip install requirements for Django rest framework.

See [credentials](./docs/credentials.md)

See [Do not do this doc](./docs/do-not-do.md)

See [notes](./docs/notes.md)