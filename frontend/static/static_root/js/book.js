function sendBookingInfo(){
	try{
				// if (isCookieSet() === false) {
				// 	alert("You need to login to book")
				// 	location.href = "/v1/login-new/"
				// 	return false
				// }
				// document.getElementById('gif-image').style.display = "block";
				console.log("Preparing to send booking details to backend server")
				// alert("Hello")

				var from = document.forms["bookingForm"]["from"].value;
				// alert(from)
				var to = document.forms["bookingForm"]["to"].value;

				var dot = document.forms["bookingForm"]["dot"].value;

				var dor = document.forms["bookingForm"]["dor"].value;

				var mem = document.getElementById("select");
				var members = mem.options[mem.selectedIndex].value

				var hour = document.getElementById("select_hour");
				var hours = hour.options[hour.selectedIndex].value
				if(!hours){
						alert("Please provide Pickup time")
						return false
				}
				var minute = document.getElementById("select_minute");
				var minutes = minute.options[minute.selectedIndex].value

				// if(!minutes){
				// 		alert("Please provide minutes too")
				// 		return false
				// }

				var period = document.getElementById("select_period");
				var periods = period.options[period.selectedIndex].value

				if (from === "" || to === "" || dot === "" || dor === "" || mem === "" || hour === "" || minute === "" || period === ""){
					alert("All the fields are required")
					return false 
				}

				var dot_arr = dot.split("/")
				console.log(dot_arr)

				var dor_arr = dor.split("/")
				console.log(dor_arr)

				d1 = new Date(dot_arr[2], dot_arr[0], dot_arr[1])
				d2 = new Date(dor_arr[2], dor_arr[0], dor_arr[1])

				console.log(d1)
				console.log(d2)
				console.log(d1<=d2)

				if(!(d1<=d2)){
					alert("Date of travel & return should be proper(D.O.T <= D.O.R)")
					return false
				}
				
	} catch(err){
		alert("Something unexpected occured" + err.message)
		return false
	}

	var contact = localStorage.getItem("contact")

	if (localStorage) {
					//Clear all keys
					
					// alert("Your browser supports localStorage");

					localStorage["from"] = from;
					localStorage["to"] = to;

					localStorage["members"] = members;

					localStorage["hours"] = hours;

					localStorage["minutes"] = minutes;

					localStorage["periods"] = periods; 

					// localStorage["booking_id"] = response.booking_id

					localStorage["dot"] = dot.split("/").join("-")

					localStorage["dor"] = dor.split("/").join("-")
					// alert("DATA SET")

	} else {
					alert("Your browser don't support localStorage, use latest Google Chrome");
	}

	console.log("Redirect link: "+"/v1/booking/vehicles-list/"+members+"/"+dot_arr.join("-")+"/"+dor_arr.join("-")+"/")
	// document.getElementById("bookingForm").reset();
	location.href = "/v1/booking/vehicles-list/"+members+"/"+dot_arr.join("-")+"/"+dor_arr.join("-")+"/"

	return 	false
}


