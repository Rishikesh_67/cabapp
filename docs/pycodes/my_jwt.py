import base64
import hashlib
import hmac
import json
# encoded = base64.b64encode('{"alg": "HS256","typ": "JWT"}')
# print encoded

header = {
  "alg": "HS256",
  "typ": "JWT"
}

enc_header = base64.b64encode(json.dumps(header))
print "Encoded Header : ", enc_header
# print encoded
# print encoded2

payload = {
  "sub": "1234567890",
  "name": "John Doe"
}

payload_enc = base64.b64encode(json.dumps(payload))

print "Encoded payload: ", payload_enc

secret = "secret"
secret_enc = base64.b64encode(secret)

signature= hmac.new("secret", enc_header+"."+payload_enc, hashlib.sha256).hexdigest()

signature2 = hmac.new(enc_header+"."+payload_enc,	 "secret", hashlib.sha256).hexdigest()

signature3= hmac.new(secret_enc, enc_header+"."+payload_enc, hashlib.sha256).hexdigest()

signature4 = hmac.new(enc_header+"."+payload_enc,	secret_enc, hashlib.sha256).hexdigest()


# # print signature
# # print signature2
# print signature3
# print signature4

print enc_header+"."+payload_enc+ "."+base64.b64encode( signature2)

print enc_header+"."+payload_enc+ "."+base64.b64encode(signature)

print enc_header+"."+payload_enc+ "."+base64.b64encode(signature3)

print enc_header+"."+payload_enc+ "."+signature4

# print base64.b64decode("Q6CM1qIz2WTgTlhMzpFL8jI8xbu9FFfj5DY_bGVY98Y")

# # from Crypto import HMAC, SHA256
# from Crypto.Hash import SHA256
# def hmac_sha256(key, msg):
#     hash_obj = HMAC.new(key=key, msg=msg, digestmod=SHA256)
#     return hash_obj.hexdigest()

# print hmac_sha256( "secret", enc_header+"."+payload_enc)
# print hmac_sha256(secret_enc, enc_header+"."+payload_enc)

# print hashlib.sha256()