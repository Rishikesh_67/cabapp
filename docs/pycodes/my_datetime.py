>>> import datetime  
>>> present = datetime.now()+datetime.timedelta(minutes=5)
Traceback (most recent call last):
  File "<console>", line 1, in <module>
AttributeError: 'module' object has no attribute 'now'
>>> present = datetime.datetime.now()+datetime.timedelta(minutes=5)
>>> present
datetime.datetime(2017, 6, 23, 12, 36, 15, 626179)
>>> present = datetime.datetime.utcnow()+datetime.timedelta(minutes=5)
>>> present
datetime.datetime(2017, 6, 23, 12, 36, 31, 328633)
>>> present = datetime.datetime.utcnow()
>>> present
datetime.datetime(2017, 6, 23, 12, 31, 51, 901358)
>>> from django.utils import timezone
>>> present = timezone.now()
>>> present
datetime.datetime(2017, 6, 23, 12, 32, 39, 378522, tzinfo=<UTC>)
>>> diff = present - past
>>> diff.total_seconds
<built-in method total_seconds of datetime.timedelta object at 0x104d8ffd0>
>>> diff.total_seconds()
370.719845
>>> diff.total_seconds()/60
6.1786640833333335